#include "SetGraph.h"
#include "cassert"


SetGraph::~SetGraph()
{

}

SetGraph::SetGraph( int size ) : IGraph(), setGraph( size )
{

}

SetGraph::SetGraph( const IGraph& graph ) : IGraph(), setGraph(graph.VerticesCount())
{
    for ( int i = 0; i < graph.VerticesCount(); ++i )
        setGraph[i] = graph.GetNextVertices( i ) ;
}

void SetGraph::AddEdge( int from, int to )
{
    assert( from >= 0 && from < setGraph.size() );
    assert( to >= 0 && to < setGraph.size() );

    setGraph[from].Add( to );
}

int SetGraph::VerticesCount() const
{
    return setGraph.size();
}

std::vector<int> SetGraph::GetNextVertices( int vertex ) const
{
    assert( vertex >= 0 && vertex < setGraph.size() );
    return setGraph[vertex].GetAllVertex();
}

std::vector<int> SetGraph::GetPrevVertices( int vertex ) const
{
    assert( vertex >= 0 && vertex < setGraph.size() );
    vector< int > result;
    for( int i = 0; i < setGraph.size(); ++i )
        if( setGraph[i].FindVertex( vertex ) )
            result.push_back( i );
    return result;
}

