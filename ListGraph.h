#ifndef TASK1_LISTGRAPH_H
#define TASK1_LISTGRAPH_H
#include "IGraph.h"
#include <vector>

using std::vector;

class ListGraph : public IGraph {
public:
    virtual ~ListGraph();
    ListGraph( int size );
    ListGraph() = delete;
    ListGraph( const IGraph& graph );
    ListGraph& operator = ( const ListGraph& graph ) = delete;
    // Добавление ребра от from к to.
    virtual void AddEdge( int from, int to ) override;

    virtual int VerticesCount() const  override;

    virtual std::vector<int> GetNextVertices( int vertex ) const override;
    virtual std::vector<int> GetPrevVertices( int vertex ) const override;

private:
    vector< vector< int > > adjacencyList;
};


#endif //TASK1_LISTGRAPH_H
