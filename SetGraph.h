#ifndef TASK1_SETGRAPH_H
#define TASK1_SETGRAPH_H
#include "IGraph.h"
#include "AVLTree.h"

class SetGraph : public IGraph{
public:
    virtual ~SetGraph();
    SetGraph() = delete;
    SetGraph( int size );
    SetGraph( const IGraph& graph );
    SetGraph& operator = ( const SetGraph& graph ) = delete;
    // Добавление ребра от from к to.
    virtual void AddEdge( int from, int to ) override;

    virtual int VerticesCount() const  override;

    virtual std::vector<int> GetNextVertices( int vertex ) const override;
    virtual std::vector<int> GetPrevVertices( int vertex ) const override;
private:

    vector< TreeAVL > setGraph;
};


#endif //TASK1_SETGRAPH_H
