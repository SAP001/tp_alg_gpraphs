#include <iostream>
#include <vector>
#include "IGraph.h"
#include "ListGraph.h"
#include "MatrixGraph.h"
#include "SetGraph.h"
#include "ArcGraph.h"
#include "queue"

using std::cout;
using std::cin;
using std::vector;
using std::queue;

int main()
{
    IGraph * graph = new ArcGraph(7);
    IGraph * graph1 = new SetGraph(7);
    IGraph * graph2 = new MatrixGraph(7);
    IGraph * graph3 = new ListGraph(7);
    //Call some methods...

    delete graph;
    delete graph1;
    delete graph2;
    delete graph3;
    return 0;
}
