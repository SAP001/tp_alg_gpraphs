#include "AVLTree.h"

TreeAVL::TreeAVL( ) : root( nullptr )
{
}

TreeAVL& TreeAVL::operator = ( const vector<int> & vec )
{
    destroyTree( root );
    root = nullptr;
    for ( auto value : vec )
        Add( value );
    return *this;
}

TreeAVL::~TreeAVL()
{
    destroyTree( root );
}

void TreeAVL::Add( const int &value )
{
    addNode(root, value );
}

void TreeAVL::Del( const int &value )
{
    deleteNode(root, value);
}

bool TreeAVL::FindVertex( int vertex ) const
{
    assert( vertex >= 0 );
    return findVertex( root, vertex );
}

vector<int> TreeAVL::GetAllVertex() const
{
    vector<int> result;
    getAllVertex( root, result );
    return result;
}

bool TreeAVL::findVertex( Node * node, int vertex ) const
{
    if( !node )
        return false;
    bool isVertex;
    if( vertex > node->value )
        isVertex = findVertex( node->right, vertex );
    else if( vertex < node->value )
        isVertex = isVertex || findVertex( node->left, vertex );
    else
        isVertex = true;

    return isVertex;
}

void TreeAVL::getAllVertex( Node * node, vector<int> & result ) const
{
    if( !node )
        return;

    getAllVertex( node->left, result );
    result.push_back( node->value );
    getAllVertex( node->right, result );
}

int TreeAVL::height( Node * tree )
{
    return !tree ? 0 : tree->height;
}

int TreeAVL::count( Node * tree )
{
    return !tree ? 0 : tree->countNods;
}

int TreeAVL::getCurrentBalance( Node * tree )
{
    return tree != nullptr ? height(tree->right)-height( tree->left ) : 0;
}

void TreeAVL::fixCount( Node * tree )
{
    if(!tree)
        return;

    int right = count(tree->right);
    int left = count(tree->left);
    tree->countNods = left + right + 1;
}

void TreeAVL::fixHeight( Node * tree )
{
    if(!tree)
        return;

    int right= height(tree->right);
    int left = height(tree->left);
    tree->height=right>left ? right+1 : left+1;
}

void TreeAVL::smallRotateRight( Node*&tree )
{
    Node*newTree=tree->left;
    tree->left=newTree->right;
    newTree->right=tree;
    fixHeight(tree);
    fixHeight(newTree);
    fixCount(tree);
    fixCount(newTree);
    tree=newTree;
}

void TreeAVL::smallRotateLeft( Node *& tree )
{
    Node*newTree=tree->right;
    tree->right=newTree->left;
    newTree->left=tree;
    fixHeight(tree);
    fixHeight(newTree);
    fixCount(tree);
    fixCount(newTree);
    tree=newTree;
}

void TreeAVL::bigRotateLeft( Node* & tree )
{
    smallRotateRight(tree->right);
    smallRotateLeft(tree);
}

void TreeAVL::bigRotateRight( Node* & tree )
{
    smallRotateLeft(tree->left);
    smallRotateRight(tree);
}

void TreeAVL::rotateLeft( Node* & tree )
{
    if (!tree)
        return;
    if (getCurrentBalance( tree->right )==-1)
        bigRotateLeft(tree);
    else
        smallRotateLeft(tree);
}

void TreeAVL::rotateRight( Node* & tree )
{
    if (!tree)
        return;
    if (getCurrentBalance(tree->left)==1)
        bigRotateRight(tree);
    else
        smallRotateRight(tree);
}

Node* TreeAVL::fixBalance( Node *& tree )
{
    fixHeight(tree);
    fixCount(tree);
    if (getCurrentBalance(tree)==-2)
        rotateRight(tree);
    else if (getCurrentBalance(tree)==2)
        rotateLeft(tree);
    return tree;
}

Node* TreeAVL::deleteNodeLeft( Node*& node )
{
    if(!node)
        return nullptr;
    if( !node->right )
    {
        Node * currentNode = node;
        node = node->left;
        return  currentNode;
    }
    else
    {
        Node * current = deleteNodeLeft(node->right);
        fixBalance(node);
        return current;
    }
}

Node* TreeAVL::deleteNodeRight( Node*& node )
{
    if(!node)
        return nullptr;
    if( !node->left )
    {
        Node * currentNode = node;
        node = node->right;
        return currentNode;
    }
    else
    {
        Node * current = deleteNodeRight(node->left);
        fixBalance(node);
        return current;
    }
}

void TreeAVL::deleteNode( Node *& tree, int value )
{
    if( !tree )
        return;
    if( tree->value <= value && value <= tree->value )
    {
        bool isLeft = height(tree->left) > height(tree->right);
        Node * newTree, *tmp;
        if( isLeft )
        {
            newTree = deleteNodeLeft(tree->left);
            if( newTree )
            {
                newTree->right = tree->right;
                newTree->left = tree->left;
            }
        }
        else
        {
            newTree = deleteNodeRight(tree->right);
            if( newTree )
            {
                newTree->left = tree->left;
                newTree->right = tree->right;
            }
        }
        tmp = tree;
        tree = newTree;
        delete tmp;
        fixBalance(tree);
        return;
    }
    deleteNode( tree->value > value ? tree->left : tree->right, value);
    fixBalance( tree);
}

void TreeAVL::destroyTree( Node * node )
{
    if( !node )
        return;
    if( node->left )
        destroyTree( node->left );
    if( node->right )
        destroyTree( node->right );

    delete node;
}

void TreeAVL::addNode( Node *& node, int value )
{
    if( !node )
    {
        node = new Node( value );
        return;
    }

    if( value > node->value )
        addNode( node->right, value );
    else
        addNode( node->left, value );

    fixBalance( node );
    fixCount( node );
}