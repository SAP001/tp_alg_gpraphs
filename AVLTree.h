#ifndef TASK1_AVLTREE_H
#define TASK1_AVLTREE_H

#include "vector"
#include "cassert"
using std::vector;

struct Node
{
    int value;
    int height;
    int countNods;
    Node * left;
    Node * right;
    Node( const int & value ): value( value ), height(1), countNods(1), left( nullptr ), right( nullptr ){};
    Node (): value( 0 ), height( 0 ), countNods( 0 ), left( nullptr ), right( nullptr ){};
};

class TreeAVL
{
public:
    TreeAVL();
    TreeAVL( const TreeAVL & ) = delete;
    TreeAVL& operator = ( const TreeAVL & ) = delete;
    TreeAVL& operator = ( const vector<int> & );
    ~TreeAVL();

    void Add( const int & value );
    bool FindVertex( int vertex ) const;
    vector<int> GetAllVertex() const;
    void Del( const int & value );

private:
    Node* deleteNodeLeft( Node *& node );
    Node* deleteNodeRight( Node *& node );
    void deleteNode( Node *& tree, int value );
    void addNode( Node *& node, int value );
    bool findVertex( Node * root, int vertex ) const;
    void getAllVertex( Node * root, vector<int> & result) const;
    void smallRotateRight( Node *& tree );
    void smallRotateLeft( Node *& tree );
    int getCurrentBalance( Node * tree );
    void bigRotateRight( Node* & tree);
    void bigRotateLeft (Node* & tree );
    void destroyTree( Node * node );
    Node* fixBalance( Node *& tree );
    void rotateRight( Node* & tree );
    void rotateLeft( Node* & tree );
    void fixHeight( Node * tree );
    void fixCount( Node * tree );
    int height( Node * tree );
    int count( Node * tree );

    Node * root;
};


#endif //TASK1_AVLTREE_H
