#ifndef TASK1_ARCGRAPH_H
#define TASK1_ARCGRAPH_H
#include "IGraph.h"

class ArcGraph : public IGraph {
public:
    virtual ~ArcGraph();
    ArcGraph() = delete;
    ArcGraph( int size );
    ArcGraph( const IGraph& graph );
    ArcGraph& operator = ( const ArcGraph& graph ) = delete;
    // Добавление ребра от from к to.
    virtual void AddEdge( int from, int to ) override;

    virtual int VerticesCount() const  override;

    virtual std::vector<int> GetNextVertices( int vertex ) const override;
    virtual std::vector<int> GetPrevVertices( int vertex ) const override;
private:
    struct Node
    {
        int from;
        int to;
        Node(): from(0), to(0){};
        Node( int f, int t ): from(f), to(t){};
    };

    int countVertex;
    vector< Node > arcGraph;
};


#endif //TASK1_ARCGRAPH_H
