#ifndef TASK1_MATRIXGRAPH_H
#define TASK1_MATRIXGRAPH_H

#include "IGraph.h"
#include <vector>

class MatrixGraph : public IGraph{
public:
    virtual ~MatrixGraph();
    MatrixGraph() = delete;
    MatrixGraph( int size );
    MatrixGraph( const IGraph& graph );
    MatrixGraph& operator = ( const MatrixGraph& graph ) = delete;
    // Добавление ребра от from к to.
    virtual void AddEdge( int from, int to ) override;

    virtual int VerticesCount() const  override;

    virtual std::vector<int> GetNextVertices( int vertex ) const override;
    virtual std::vector<int> GetPrevVertices( int vertex ) const override;
private:
    vector< vector< int > > matrix;
};


#endif //TASK1_MATRIXGRAPH_H
