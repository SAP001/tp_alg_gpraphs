#include "MatrixGraph.h"
#include "cassert"


MatrixGraph::~MatrixGraph()
{
}

MatrixGraph::MatrixGraph( int size ) : IGraph(), matrix( size )
{
    for( int i = 0; i < size; ++i )
        matrix[i].resize(size, IS_NO_WAY );
}

MatrixGraph::MatrixGraph( const IGraph& graph ) : IGraph()
{
    matrix.resize( graph.VerticesCount() );
    for (int i = 0; i < graph.VerticesCount(); ++i)
        matrix[i] = graph.GetNextVertices( i );
}

void MatrixGraph::AddEdge( int from, int to )
{
    assert( from >= 0 && from < matrix.size() );
    assert( to >= 0 && to < matrix.size() );

    matrix[from][to] = IS_A_WAY;
}

int MatrixGraph::VerticesCount() const
{
    return matrix.size();
}

std::vector<int> MatrixGraph::GetNextVertices( int vertex ) const
{
    assert( vertex >= 0 && vertex < matrix.size() );
    vector<int> result;
    for( int i = 0; i < matrix.size(); ++i )
        if( matrix[vertex][i] == IS_A_WAY )
            result.push_back( i );
    return result;
}

std::vector<int> MatrixGraph::GetPrevVertices( int vertex ) const
{
    assert( vertex >= 0 && vertex < matrix.size() );
    vector< int > result;
    for( int i = 0; i < matrix.size(); ++i )
        if( matrix[i][vertex] == IS_A_WAY )
            result.push_back( i );
    return result;
}

