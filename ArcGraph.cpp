#include "ArcGraph.h"
#include "cassert"


ArcGraph::~ArcGraph()
{
}

ArcGraph::ArcGraph( int size ) : IGraph(), countVertex( size ), arcGraph(0)
{

}

ArcGraph::ArcGraph( const IGraph& graph ) : IGraph(), countVertex(graph.VerticesCount())
{
    arcGraph.clear();
    vector<int> tmp;
    for (int from = 0; from < countVertex; ++from)
    {
        tmp = graph.GetNextVertices( from );
        for( auto to : tmp )
        {
            arcGraph.emplace_back( from, to );
        }
    }
}

void ArcGraph::AddEdge( int from, int to )
{
    assert( from >= 0 && from < countVertex );
    assert( to >= 0 && to < countVertex );

    ++countVertex;
    arcGraph.emplace_back( from, to );
}

int ArcGraph::VerticesCount() const
{
    return countVertex;
}

std::vector<int> ArcGraph::GetNextVertices( int vertex ) const
{
    assert( vertex >= 0 && vertex < countVertex );
    vector<int> result;
    for( int i = 0; i < arcGraph.size(); ++i )
        if( arcGraph[i].from == vertex )
            result.push_back( arcGraph[i].to );
    return result;
}

std::vector<int> ArcGraph::GetPrevVertices( int vertex ) const
{
    assert( vertex >= 0 && vertex < countVertex );
    vector<int> result;
    for( int i = 0; i < arcGraph.size(); ++i )
        if( arcGraph[i].to == vertex )
            result.push_back( arcGraph[i].from );
    return result;
}

