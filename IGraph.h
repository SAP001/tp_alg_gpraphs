#ifndef TASK1_IGRAPH_H
#define TASK1_IGRAPH_H
#include <vector>
#include "AVLTree.h"
#define IS_NO_WAY 0
#define IS_A_WAY 1
using std::vector;
struct IGraph {
    virtual ~IGraph() {}
    // Добавление ребра от from к to.
    virtual void AddEdge( int from, int to ) = 0;

    virtual int VerticesCount() const  = 0;

    virtual std::vector<int> GetNextVertices( int vertex ) const = 0;
    virtual std::vector<int> GetPrevVertices( int vertex ) const = 0;
};

#endif //TASK1_IGRAPH_H
