#include "ListGraph.h"
#include "cassert"


ListGraph::~ListGraph()
{}

ListGraph::ListGraph( int size ) : IGraph(), adjacencyList( size )
{

}

ListGraph::ListGraph( const IGraph& graph ) : IGraph()
{
    adjacencyList.resize( graph.VerticesCount() );
    for( int from = 0; from < graph.VerticesCount(); ++from )
        adjacencyList[from] = graph.GetNextVertices( from );
}

void ListGraph::AddEdge( int from, int to )
{
    assert( from >= 0 && from < adjacencyList.size() );
    assert( to >= 0 && to < adjacencyList.size() );

    adjacencyList[from].push_back(to);
}

int ListGraph::VerticesCount() const
{
    return adjacencyList.size();
}

std::vector<int> ListGraph::GetNextVertices( int vertex ) const
{
    assert( vertex >= 0 && vertex < adjacencyList.size() );
    return adjacencyList[vertex];
}

std::vector<int> ListGraph::GetPrevVertices( int vertex ) const
{
    assert( vertex >= 0 && vertex < adjacencyList.size() );
    vector< int > result;
    for( int from = 0; from < adjacencyList.size(); ++from )
        for( int i = 0; i < adjacencyList[from].size(); ++i )
            if( adjacencyList[from][i] == vertex )
                result.push_back( from );
    return result;
}

